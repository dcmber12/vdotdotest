#include <android/log.h>

#include <jni.h>

#include <string>
#include <vector>

#include "ncnn/net.h"

#include <sys/time.h>
#include <assert.h>

#define LOG_TAG "NativeNetwork"
#define IMG_H 227
#define IMG_W 227
#define IMG_C 9
#define MAX_DATA_SIZE IMG_H * IMG_W * IMG_C

static void bench_start();
static void bench_end(const char* comment);
static std::vector<std::string> split_string(const std::string& str, const std::string& delimiter);

static struct timeval tv_begin;
static struct timeval tv_end;
static double elasped;

static std::vector<unsigned char> squeezenet_param;
static std::vector<unsigned char> squeezenet_bin;
static std::vector<std::string> squeezenet_words;
static ncnn::Net squeezenet;



extern "C" {
JNIEXPORT jboolean JNICALL
Java_test_vdo_com_vdotdotest_model_NeuralNetwork_initNCNN(
        JNIEnv *env, jobject thiz, jbyteArray param, jbyteArray bin, jbyteArray words) {

    // init param
    {
        int len = env->GetArrayLength(param);
        squeezenet_param.resize(len);
        env->GetByteArrayRegion(param, 0, len, (jbyte *) squeezenet_param.data());
        int ret = squeezenet.load_param(squeezenet_param.data());
        __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "load_param %d %d", ret, len);
    }

    // init bin
    {
        int len = env->GetArrayLength(bin);
        squeezenet_bin.resize(len);
        env->GetByteArrayRegion(bin, 0, len, (jbyte *) squeezenet_bin.data());
        int ret = squeezenet.load_model(squeezenet_bin.data());
        __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "load_model %d %d", ret, len);
    }

    // init words
    {
        int len = env->GetArrayLength(words);
        std::string words_buffer;
        words_buffer.resize(len);
        env->GetByteArrayRegion(words, 0, len, (jbyte *) words_buffer.data());
        squeezenet_words = split_string(words_buffer, "\n");
    }

    return JNI_TRUE;
}
}


extern "C" {
JNIEXPORT void JNICALL
Java_test_vdo_com_vdotdotest_model_NeuralNetwork_yuv2rgbNative(JNIEnv *env, jobject obj,
                                                         jfloatArray rgb,
                                                         jbyteArray y_array,
                                                         jbyteArray u_array,
                                                         jbyteArray v_array,
                                                         jint width,
                                                         jint height,
                                                         jint rowStride, jint pixelStride) {

    float rgb_array[IMG_H*IMG_W*3];
    jsize Y_len = env->GetArrayLength(y_array);
    jbyte * Y_data = env->GetByteArrayElements(y_array, 0);
    assert(Y_len <= MAX_DATA_SIZE);
    jsize U_len = env->GetArrayLength(u_array);
    jbyte * U_data = env->GetByteArrayElements(u_array, 0);
    assert(U_len <= MAX_DATA_SIZE);
    jsize V_len = env->GetArrayLength(v_array);
    jbyte * V_data = env->GetByteArrayElements(v_array, 0);
    assert(V_len <= MAX_DATA_SIZE);

#define min(a,b) ((a) > (b)) ? (b) : (a)
#define max(a,b) ((a) > (b)) ? (a) : (b)

    auto h_offset = max(0, (height - IMG_H) / 2);
    auto w_offset = max(0, (width - IMG_W) / 2);

    auto iter_h = IMG_H;
    auto iter_w = IMG_W;
    if (height < IMG_H) {
        iter_h = height;
    }
    if (width < IMG_W) {
        iter_w = width;
    }

    for (auto i = 0; i < iter_h; ++i) {
        jbyte *Y_row = &Y_data[(h_offset + i) * width];
        jbyte *U_row = &U_data[(h_offset + i) / 4 * rowStride];
        jbyte *V_row = &V_data[(h_offset + i) / 4 * rowStride];
        for (auto j = 0; j < iter_w; ++j) {
            // Tested on Pixel and S7.
            char y = Y_row[w_offset + j];
            char u = U_row[pixelStride * ((w_offset + j) / pixelStride)];
            char v = V_row[pixelStride * ((w_offset + j) / pixelStride)];

            float b_mean = 104.00698793f;
            float g_mean = 116.66876762f;
            float r_mean = 122.67891434f;

            auto b_i = 0 * IMG_H * IMG_W + j * IMG_W + i;
            auto g_i = 1 * IMG_H * IMG_W + j * IMG_W + i;
            auto r_i = 2 * IMG_H * IMG_W + j * IMG_W + i;


//              For 3 channel image
//              R = Y + 1.402 (V-128)
//              G = Y - 0.34414 (U-128) - 0.71414 (V-128)
//              B = Y + 1.772 (U-V)

            rgb_array[r_i] =
                    -r_mean + (float) ((float) min(255., max(0., (float) (y + 1.402 * (v - 128)))));
            rgb_array[g_i] =
                    -g_mean + (float) ((float) min(255., max(0., (float) (y - 0.34414 *
                                                                                        (u - 128) -
                                                                                    0.71414 *
                                                                                    (v - 128)))));
            rgb_array[b_i] =
                    -b_mean + (float) ((float) min(255., max(0., (float) (y + 1.772 * (u - v)))));
        }
    }

    env->SetFloatArrayRegion(rgb, 0, IMG_W*IMG_H*3, &rgb_array[0]);
    env->ReleaseByteArrayElements(y_array, Y_data, JNI_ABORT);
    env->ReleaseByteArrayElements(u_array, U_data, JNI_ABORT);
    env->ReleaseByteArrayElements(v_array, V_data, JNI_ABORT);
}
}

extern "C" {
JNIEXPORT jstring JNICALL
Java_test_vdo_com_vdotdotest_model_NeuralNetwork_detect(JNIEnv *env,
                                                        jobject thiz,
                                                        jint n,
                                                        jfloatArray rgb,
                                                        jint rowStride, jint pixelStride) {
// YUV, YUV, YUV ....

//    bench_start();

//    float input_data[h*w*c];

//    jbyte * YUV_data = env->GetByteArrayElements(YUV, 0);
//    jsize all_len = env->GetArrayLength(YUV);
//    assert(all_len == (y_len + 2*uv_len) * n);

//    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "Y length : %d", y_len);
//    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "UV length : %d", uv_len);
//    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "all batch length : %d", all_len);


//
//
////    ncnn::Mat in = ncnn::Mat::from_pixels((const unsigned char*)input_data,
////                                          ncnn::Mat::PIXEL_RGB2BGR, w, h);
//
//    bench_end("preprocessing");
//
//    bench_start();
//
//    // squeezenet
//    std::vector<float> cls_scores;
//    {
//        const float mean_vals[3] = {104.f, 117.f, 123.f};
//        in.substract_mean_normalize(mean_vals, 0);
//
//        ncnn::Extractor ex = squeezenet.create_extractor();
//        ex.set_light_mode(true);
//        ex.set_num_threads(4);
//
//        ex.input(alexnet_param_id::BLOB_data, in);
//        ncnn::Mat out;
//        ex.extract(alexnet_param_id::BLOB_prob, out);
//
//        cls_scores.resize(out.c);
//        for (int j = 0; j < out.c; j++) {
//            const float *prob = out.data + out.cstep * j;
//            cls_scores[j] = prob[0];
//        }
//    }
//
//    // return top class
//    int top_class = 0;
//    float max_score = 0.f;
//    for (size_t i = 0; i < cls_scores.size(); i++) {
//        float s = cls_scores[i];
//        if (s > max_score) {
//            top_class = i;
//            max_score = s;
//        }
//    }
//
//    const std::string &word = squeezenet_words[top_class];
//    char tmp[32];
//    sprintf(tmp, "%.3f", max_score);
//    std::string result_str = std::string(word.c_str() + 10) + " = " + tmp;
//
//    // +10 to skip leading n03179701
//    jstring result = env->NewStringUTF(result_str.c_str());
//
//    bench_end("forwarded.");
//
//    return result;
//    */
    return env->NewStringUTF("fefefef");
}
}





static void bench_start()
{
    gettimeofday(&tv_begin, NULL);
}

static void bench_end(const char* comment)
{
    gettimeofday(&tv_end, NULL);
    elasped = ((tv_end.tv_sec - tv_begin.tv_sec) * 1000000.0f + tv_end.tv_usec - tv_begin.tv_usec) / 1000.0f;
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "%.4fms   %s", elasped, comment);
}

static std::vector<std::string> split_string(const std::string& str, const std::string& delimiter)
{
    std::vector<std::string> strings;

    std::string::size_type pos = 0;
    std::string::size_type prev = 0;
    while ((pos = str.find(delimiter, prev)) != std::string::npos)
    {
        strings.push_back(str.substr(prev, pos - prev));
        prev = pos + 1;
    }

    // To get the last substring (or only, if delimiter is not found)
    strings.push_back(str.substr(prev));

    return strings;
}


#include "batchconvolution.h"
#include "ncnn/convolution.h"
#include "math.h"
extern "C" {
JNIEXPORT jboolean JNICALL
Java_test_vdo_com_vdotdotest_activity_BatchConvActivity_testBatchConv(
        JNIEnv *env, jobject thiz) {

    // layer params
    ncnn::BatchConvolution convolution_layer;
    convolution_layer.num_output = 16;
    convolution_layer.kernel_w = 3;
    convolution_layer.kernel_h = 3;
    convolution_layer.dilation_w = 1;
    convolution_layer.dilation_h = 1;
    convolution_layer.stride_w = 1;
    convolution_layer.stride_h = 1;
    convolution_layer.pad_w = 0;
    convolution_layer.pad_h = 0;
    convolution_layer.bias_term = 1;
    convolution_layer.weight_data_size = 432;
    convolution_layer.group = 1;

    int size = 112;
    int batch_n = 7;

    // weights & bias
    float_t weight[432];
    for (int i=0; i< 432 ; i++)
    {
        weight[i] = 1.0f;
    }

    float_t bias[] = {
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
    };

    // forward
//    ncnn::Mat mat_in(size, size, 3, in);
    ncnn::Mat mat_in;
    mat_in.create(batch_n, size, size, 3);
    for (int i = 0; i < batch_n*size*size*3; ++i)
    {
        mat_in[i] = 1.0;
    }
    ncnn::Mat mat_out;

    convolution_layer.bias_data.data = bias;
    convolution_layer.weight_data.data = weight;
    bench_start();
    convolution_layer.forward(mat_in, mat_out);
    bench_end(" forwarded.");

    ncnn::Mat mat_expected;
    mat_expected.create(batch_n, size-2, size-2, 16);
    for (int i = 0; i < batch_n*(size-2)*(size-2)*16; ++i)
    {
        mat_expected[i] = 27.0;
    }

    int expected_output_size = (size-2)*(size-2)*batch_n*16;
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "output size : %d", expected_output_size);
    int test = 0;
    for (int i = 0; i < expected_output_size; ++i)
    {
        if (mat_out[i] - mat_expected[i] > 0.00001)
            test++;
    }
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "test diffrence : %d", test);


    return JNI_TRUE;
}
}


