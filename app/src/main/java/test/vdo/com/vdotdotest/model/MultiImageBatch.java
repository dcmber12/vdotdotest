package test.vdo.com.vdotdotest.model;

import android.util.Log;

import org.apache.commons.lang3.ArrayUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Created by leesangho on 2018. 1. 29..
 */

public class MultiImageBatch extends LinkedList<ImageBatch> {

    public static final int MIN_QUEUE_INTERVAL = 8;
    public static final int QUEUE_SIZE = 3;
    private static final String TAG = MultiImageBatch.class.getSimpleName();

    private int w = -1;
    private int h = -1;
    private int c = -1;
    private int rowStride = -1;
    private int pixelStride = -1;

    private byte[] multi_yuv;
    private float[] multi_rgb;

    void yuv2rgb() {
        for (ImageBatch img : this) {
            img.yuv2rgb();
        }
    }

    /**
     * Concatting the data array
     * @throws IOException
     */
    void mergeRGB() throws IOException {
        this.multi_rgb = null;
        for (ImageBatch img : this) {
            this.multi_rgb = ArrayUtils.addAll(this.multi_rgb, img.getRgb());
            Log.d(TAG, "rgb added : " + this.multi_rgb.length);
        }
    }


    int getYlen() {
        return getFirst().getY().length;
    }

    int getUVlen() {
        int ulen = getFirst().getU().length;
        if (ulen != getFirst().getV().length)
            throw new IllegalStateException();
        return ulen;
    }

//    /**
//     * only for debugging
//     * @throws IOException
//     */
//    void onlyLastOne() throws IOException {
//
//        this.multi_y = getLast().getY();
//        this.multi_u = getLast().getU();
//        this.multi_v = getLast().getV();
//    }

    @Override
    public void addLast(ImageBatch imageBatch) {

        if (!imageBatch.isValid())
            throw new IllegalArgumentException();

        if (size() > 0) {
            if (this.w != imageBatch.getW() || this.h != imageBatch.getH()
                    || this.c != imageBatch.getC()
                    || this.pixelStride != imageBatch.getPixelStride()
                    || this.rowStride != imageBatch.getRowStride())
                    throw new IllegalArgumentException();
        } else {
            this.w = imageBatch.getW();
            this.h = imageBatch.getH();
            this.c = imageBatch.getC();
            this.pixelStride = imageBatch.getPixelStride();
            this.rowStride = imageBatch.getRowStride();
        }
        super.addLast(imageBatch);
    }


    public int getRowStride() {
        return rowStride;
    }

    public void setRowStride(int rowStride) {
        this.rowStride = rowStride;
    }

    public int getPixelStride() {
        return pixelStride;
    }

    public void setPixelStride(int pixelStride) {
        this.pixelStride = pixelStride;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public byte[] getMulti_yuv() {
        return multi_yuv;
    }

    public void setMulti_yuv(byte[] multi_yuv) {
        this.multi_yuv = multi_yuv;
    }

    public float[] getMulti_rgb() {
        return multi_rgb;
    }

    public void setMulti_rgb(float[] multi_rgb) {
        this.multi_rgb = multi_rgb;
    }


    //    void mergeYxUxVx() throws IOException {
//
//        ByteArrayOutputStream baos_y = new ByteArrayOutputStream();
//        ByteArrayOutputStream baos_u = new ByteArrayOutputStream();
//        ByteArrayOutputStream baos_v = new ByteArrayOutputStream();
//        for (ImageBatch img : this) {
//            baos_y.write(img.getY());
//            baos_u.write(img.getU());
//            baos_v.write(img.getV());
//        }
//        this.multi_y = baos_y.toByteArray();
//        this.multi_u = baos_u.toByteArray();
//        this.multi_v = baos_v.toByteArray();
//        baos_y.close();
//        baos_u.close();
//        baos_v.close();
//    }
//
//    public byte[] getMulti_y() {
//        return multi_y;
//    }
//
//    public void setMulti_y(byte[] multi_y) {
//        this.multi_y = multi_y;
//    }
//
//    public byte[] getMulti_u() {
//        return multi_u;
//    }
//
//    public void setMulti_u(byte[] multi_u) {
//        this.multi_u = multi_u;
//    }
//
//    public byte[] getMulti_v() {
//        return multi_v;
//    }
//
//    public void setMulti_v(byte[] multi_v) {
//        this.multi_v = multi_v;
//    }


//    MultiImageBatch() {
//        for (Image img : mImage) {
//
//            if (!isValidImage(img))
//                throw new IllegalArgumentException();
//
//            if (this.w == -1)
//                this.w = img.getWidth();
//            if (this.h == -1)
//                this.h = img.getHeight();
//            if (this.pixelStride == -1)
//                this.pixelStride = img.getPlanes()[1].getPixelStride();
//            if (this.rowStride == -1)
//                this.rowStride = img.getPlanes()[1].getRowStride();
//
//            if (this.w != img.getWidth() || this.h != img.getHeight()
//                    || this.pixelStride != img.getPlanes()[1].getPixelStride()
//                    || this.rowStride != img.getPlanes()[1].getRowStride())
//                throw new IllegalArgumentException();
//        }
//        this.c = mImage.size() * 3;
//    }

}
