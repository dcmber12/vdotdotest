package test.vdo.com.vdotdotest.listener;

/**
 * Created by leesangho on 2018. 2. 14..
 */

public interface NetworkResultListener  {

    void onNetworkResult(String result);

}
