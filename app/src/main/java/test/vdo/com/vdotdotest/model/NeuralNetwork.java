package test.vdo.com.vdotdotest.model;

import android.content.Context;
import android.media.Image;
import android.util.Log;

import com.arasthel.asyncjob.AsyncJob;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.LinkedList;

import test.vdo.com.vdotdotest.listener.NetworkResultListener;

/**
 * {@link NeuralNetwork} is an wrapper class of native cpp ncnn things
 * Forwarding is not thread safe, using one thread environment only
 *
 * Created by leesangho on 2018. 1. 29..
 */

public class NeuralNetwork implements Serializable {

    private static final String TAG = NeuralNetwork.class.getSimpleName();
    private static volatile NeuralNetwork sSingleInstance;
    private boolean isNetworkInit;
    private boolean isProcessing;

    public static final int width = 227;
    public static final int height = 227;

    public native boolean initNCNN(byte[] param, byte[] bin, byte[] words);
//    public native String detect(int h, int w, int c, int n,
//                                byte[] Y, byte[] U, byte[] V,
//                                int rowStride, int pixelStride);
    public native String detect(int n,
                                float[] rgb,
                                int rowStride, int pixelStride);

    public native void yuv2rgbNative(float[] rgb, byte[] y, byte[] u, byte[] v,
                                 int width, int height, int rowStride, int pixelStride);


    // make private constructor to restricte make new instance.
    private NeuralNetwork(){

        // initialize members
        this.isNetworkInit = false;

        //Prevent form the reflection api.
        if (sSingleInstance != null){
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    //Make singleton from serialize and deserialize operation.
    protected NeuralNetwork readResolve() {
        return getInstance();
    }

    /**
     * Load library when startups.
     */
    public static void loadLibrary(){
        System.loadLibrary("native-lib");
    }

    /**
     * Thread safe getinstance.
     * https://android.jlelse.eu/how-to-make-the-perfect-singleton-de6b951dfdb0
     *
     * @return get or new Instance
     */
    public static NeuralNetwork getInstance() {

        //Double check locking pattern
        if (sSingleInstance == null) {
            synchronized (NeuralNetwork.class) {
                if (sSingleInstance == null) {
                    sSingleInstance = new NeuralNetwork();
                }
            }
        }

        return sSingleInstance;
    }

    /**
     * Check network is initialized.
     * @return
     */
    public boolean isInitialized() {
        return isNetworkInit;
    }


    /**
     * Check if network processing.
     * @return
     */
    public boolean isNetworkProcessing(){
        return isProcessing;
    }

    /**
     * set this network is processing.
     */
    public void setIsProcess(boolean isProcess) {
        this.isProcessing = isProcess;
    }

    /**
     * Initialize network synchronously, Please check UI overhead
     * @return
     */
    public boolean init(Context context) throws IOException {

        if (isInitialized()) {
            Log.d(TAG, "Network already initialized.");
            return true;
        }

        byte[] param = getAssetByteArray(context, "squeezenet_v1.1.param.bin");
        byte[] bin = getAssetByteArray(context, "squeezenet_v1.1.bin");
        byte[] words = getAssetByteArray(context, "synset_words.txt");
        isNetworkInit = initNCNN(param, bin, words);
        return isNetworkInit;
    }

    public float[] yuv2rgb(int w, int h,
                          byte[] y, byte[] u, byte[] v,
                          int rowStride, int pixelStride) {
        float[] rgb = new float[227*227*3]; //TODO
        yuv2rgbNative(rgb, y, u, v, w, h, rowStride, pixelStride);
        return rgb;
    }


    /**
     * Forward network asynchronously
     * @param frames
     * @return
     * @throws IOException
     */
    public void forwardAsync(final MultiImageBatch frames, final NetworkResultListener listener) throws IOException {
        if (frames.size() < MultiImageBatch.QUEUE_SIZE)
            throw new IllegalStateException();
        frames.yuv2rgb();
        frames.mergeRGB();
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {
                setIsProcess(false);
                //TODO
                Log.d(TAG, "rgb size (expected " + String.valueOf(227*227*3*3) + ") : " + frames.getMulti_rgb().length);
                String result = detect(frames.size(),
                            frames.getMulti_rgb(),
                            frames.getRowStride(), frames.getPixelStride());
                listener.onNetworkResult(result);
            }
        });
    }



    /**
     * Get file byte array by name from asset
     * @param context
     * @param filename
     * @return byte[]
     */
    private byte[] getAssetByteArray(Context context, String filename) throws IOException{
        InputStream assetsInputStream = context.getAssets().open(filename);
        int available = assetsInputStream.available();
        byte[] result = new byte[available];
        if (result.length != available)
            throw new IllegalStateException();
        assetsInputStream.close();
        return result;
    }

}
