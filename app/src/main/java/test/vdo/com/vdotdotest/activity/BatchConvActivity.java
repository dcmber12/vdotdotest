package test.vdo.com.vdotdotest.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import test.vdo.com.vdotdotest.R;

/**
 * Created by leesangho on 2018. 1. 31..
 */

public class BatchConvActivity extends AppCompatActivity {

    private static final String TAG = BatchConvActivity.class.getSimpleName();
    @BindView(R.id.act_batch_conv_btn)
    Button mBtn;

    public native boolean testBatchConv();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_batch_conv);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.act_batch_conv_btn)
    public void onClickBtn() {
        BatchConv task = new BatchConv();
        task.execute();
    }


    class BatchConv extends  AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            boolean res = testBatchConv();
            Log.d(TAG, "Test Success!!!");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(BatchConvActivity.this, "Test success.", Toast.LENGTH_SHORT).show();
                }
            });
            return null;
        }
    }




}
