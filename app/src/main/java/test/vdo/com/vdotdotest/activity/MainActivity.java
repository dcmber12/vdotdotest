package test.vdo.com.vdotdotest.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.vdo.com.vdotdotest.R;
import test.vdo.com.vdotdotest.listener.NetworkResultListener;
import test.vdo.com.vdotdotest.model.ImageBatch;
import test.vdo.com.vdotdotest.model.MultiImageBatch;
import test.vdo.com.vdotdotest.model.NeuralNetwork;


/**
 * Created by leesangho on 2018. 1. 29..
 */

public class MainActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener,
        ImageReader.OnImageAvailableListener, NetworkResultListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_CAMERA_PERMISSION = 100;

    @BindView(R.id.act_main_textureView)
    TextureView mTexture;

    @BindView(R.id.act_main_sample_text)
    TextView mTextView;

    private Size mImageDimension;

    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest.Builder captureRequestBuilder;

    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private Image image;  // This cannot be local variable in callback method. strange.
    private MultiImageBatch mFrameQueue; //Note that this implementation is not synchronized.
    private int que_interval;

    private final MainActCameraStateCallback stateCallback = new MainActCameraStateCallback();
    private static final Object FRAME_QUEUE_SYNCHRO_LOCK = new Object();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Full screen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        // adding ContentView
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        // TODO : Asynchronously load
        // Initialize Network Sync
        boolean res = false;
        try {
            res = NeuralNetwork.getInstance().init(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!res)
            Toast.makeText(this, "Network init failed.", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Network init success.", Toast.LENGTH_SHORT).show();

        // View init
        mTexture.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE);
        mTextView = findViewById(R.id.act_main_sample_text);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mTexture.isAvailable()) {
            openCamera();
        } else {
            mTexture.setSurfaceTextureListener(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(MainActivity.this, "You can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "opPause() called.");
        closeCamera();
        finalizedStates();
        super.onPause();
    }

    // ---------------------------------------------------------------------------------------------
    // Implementing TextureView.SurfaceTextureListener
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        openCamera();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }

    // ---------------------------------------------------------------------------------------------
    // Implementing CameraDevice.StateCallback()
    // ---------------------------------------------------------------------------------------------
    private class MainActCameraStateCallback extends CameraDevice.StateCallback {
        @Override
        public void onOpened(CameraDevice camera) {
            cameraDevice = camera;
            initializeStates();
            createCameraPreview(mBackgroundHandler);
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
            Log.d(TAG, "camera is disconnected.");
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            Toast.makeText(MainActivity.this, "Camera is not available!!!", Toast.LENGTH_LONG).show();
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Implementing ImageReader.OnImageAvailableListener()
    // ---------------------------------------------------------------------------------------------
    /**
     * This method is called from {@link HandlerThread} Handler
     * @param reader
     */
    @Override
    public void onImageAvailable(ImageReader reader) {
        try {

            // TODO : Test Thread Safety
            synchronized (FRAME_QUEUE_SYNCHRO_LOCK) {

                if (MainActivity.this.getBaseContext() == null)
                    return;

                // Get next image
                image = reader.acquireNextImage();
                if (image == null)
                    return;
                ImageBatch batch = new ImageBatch(image);
                image.close();

                // Adjusting Queue
                if (mFrameQueue.size() < MultiImageBatch.QUEUE_SIZE) { // First three images.
                    mFrameQueue.addLast(batch);
                    Log.d(TAG, "first frame group added");

                    if (mFrameQueue.size() < MultiImageBatch.QUEUE_SIZE)
                        return;

                } else if (mFrameQueue.size() == MultiImageBatch.QUEUE_SIZE) { // Idle states.

                    //Checking Queue interval
                    que_interval++;
                    if (que_interval < MultiImageBatch.MIN_QUEUE_INTERVAL) {
//                    Log.d(TAG, "frame skipped : " + que_interval);
                        return;
                    }

                    // FIFO
                    que_interval = 0;
                    mFrameQueue.removeFirst(); // removeFirst() throws No such element exception if empty
                    mFrameQueue.addLast(batch);
                    Log.d(TAG, "frame added in FIFO order.");
                } else {
                    throw new IllegalStateException("Illegal Queue size");
                }

                // Check if forwarding
                if (NeuralNetwork.getInstance().isNetworkProcessing()) {
                    Log.d(TAG, "Network is already processing....");
                    return;
                } else {
                    NeuralNetwork.getInstance().setIsProcess(true);  //set this before asynchronously start.
                    NeuralNetwork.getInstance().forwardAsync(mFrameQueue, this);
                }
                Log.d(TAG, "frame is forwarding.");
            }

        } catch (IOException e) {
            NeuralNetwork.getInstance().setIsProcess(false);
            e.printStackTrace();
            Log.e(TAG, "Image IO Exception", e);
        }
    }
    // ---------------------------------------------------------------------------------------------
    // Implementing Neural Network Result listener
    // ---------------------------------------------------------------------------------------------
    @Override
    public void onNetworkResult(final String result) {

        if (MainActivity.this.getBaseContext() == null)
            return;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Note this is not 100% sync with Network output
                mTextView.setText(result);
            }
        });
    }


    // ---------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    // ---------------------------------------------------------------------------------------------
    /**
     * Called when Surface is ready.
     */
    private void openCamera() {
        Log.d(TAG, "openCamera() called.");
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        String cameraId;
        try {
            cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            mImageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    /**
     * Called when camera is opened.
     */
    private void createCameraPreview(Handler handler) {

        if (handler.getLooper() == Looper.getMainLooper())
            throw new IllegalStateException("called createCameraPreview() with uninitialize camera frame thread");

        try {

            // Set surface buffer size
            SurfaceTexture texture = mTexture.getSurfaceTexture();
            texture.setDefaultBufferSize(mImageDimension.getWidth(), mImageDimension.getHeight());
            Surface surface = new Surface(texture);

            // Create image reader and listener
            ImageReader reader = ImageReader.newInstance(NeuralNetwork.width, NeuralNetwork.height,
                    ImageFormat.YUV_420_888, 4);

            reader.setOnImageAvailableListener(MainActivity.this, handler);

            // Set capture request
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            captureRequestBuilder.addTarget(reader.getSurface());

            // Configuration Change may not need to implemented.
            cameraDevice.createCaptureSession(Arrays.asList(surface, reader.getSurface()), new CameraCaptureSession.StateCallback(){
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (cameraDevice == null) {
                        return;
                    }
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }
                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(MainActivity.this, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * initialize states.
     */
    protected void initializeStates() {
        if (mFrameQueue == null) {
            mFrameQueue = new MultiImageBatch();
        }

        if (mBackgroundThread == null) {
            que_interval = 0;
            mBackgroundThread = new CameraFrameThread();
            mBackgroundThread.start();
            mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
        }
    }

    /**
     * kill the frame handler thread.
     */
    protected void finalizedStates() {
        que_interval = 0;
        mFrameQueue = null;
        mBackgroundThread.quitSafely();
        try {
            // TODO : join() is needing?
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void updatePreview() {
        if(null == cameraDevice) {
            return;
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private class CameraFrameThread extends HandlerThread {

        public CameraFrameThread() {
            super("Camera Background");
        }

        public CameraFrameThread(int priority) {
            super(CameraFrameThread.class.getSimpleName(), priority);
        }
    }

}

