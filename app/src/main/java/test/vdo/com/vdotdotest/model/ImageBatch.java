package test.vdo.com.vdotdotest.model;

import android.media.Image;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by leesangho on 2018. 1. 30..
 */

public class ImageBatch {

    private int w = -1;
    private int h = -1;
    private int c = -1;
    private int rowStride = -1;  // for U, V
    private int pixelStride = -1; // for U, V

    private byte[] y;
    private byte[] u;
    private byte[] v;

    private byte[] yuv;
    private float[] rgb;

    public ImageBatch(Image image) {

        this.w = image.getWidth();
        this.h = image.getHeight();
        this.c = 3;
        this.pixelStride = image.getPlanes()[1].getPixelStride();
        this.rowStride = image.getPlanes()[1].getRowStride();

        ByteBuffer Ybuffer = image.getPlanes()[0].getBuffer();
        ByteBuffer Ubuffer = image.getPlanes()[1].getBuffer();
        ByteBuffer Vbuffer = image.getPlanes()[2].getBuffer();
        this.y = new byte[Ybuffer.capacity()];
        this.u = new byte[Ubuffer.capacity()];
        this.v = new byte[Vbuffer.capacity()];
        Ybuffer.get(this.y);
        Ubuffer.get(this.u);
        Vbuffer.get(this.v);
    }

    void mergeYUV() throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(this.y);
        baos.write(this.u);
        baos.write(this.v);
        this.yuv = baos.toByteArray();
        baos.close();
    }

    void yuv2rgb() {
        this.rgb = NeuralNetwork.getInstance().yuv2rgb(
                this.w, this.h,
                this.y, this.u, this.v,
                 this.rowStride, this.pixelStride);
    }

    boolean isValid() {
        return (getW() > 0 && getH() > 0 && getC() > 0);
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }


    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getRowStride() {
        return rowStride;
    }

    public void setRowStride(int rowStride) {
        this.rowStride = rowStride;
    }

    public int getPixelStride() {
        return pixelStride;
    }

    public void setPixelStride(int pixelStride) {
        this.pixelStride = pixelStride;
    }

    public byte[] getY() {
        return y;
    }

    public void setY(byte[] y) {
        this.y = y;
    }

    public byte[] getU() {
        return u;
    }

    public void setU(byte[] u) {
        this.u = u;
    }

    public byte[] getV() {
        return v;
    }

    public void setV(byte[] v) {
        this.v = v;
    }

    public byte[] getYUV() {
        return yuv;
    }

    public void setYUV(byte[] yuv) {
        this.yuv = yuv;
    }

    public float[] getRgb() {
        return rgb;
    }

    public void setRgb(float[] rgb) {
        this.rgb = rgb;
    }
}
