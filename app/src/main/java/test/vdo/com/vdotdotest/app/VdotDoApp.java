package test.vdo.com.vdotdotest.app;

import android.app.Application;

import test.vdo.com.vdotdotest.model.NeuralNetwork;

/**
 * Created by leesangho on 2018. 1. 29..
 */

public class VdotDoApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        // load shared library
        NeuralNetwork.loadLibrary();

    }
}
